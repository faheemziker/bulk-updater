import os
import config
from flask import Flask, request, jsonify, flash, redirect
from bulkupdater import BulkUpdater
from werkzeug.utils import secure_filename

app = Flask(__name__)

updater = BulkUpdater()

UPLOAD_FOLDER = config.get("system","FILE_PATH")
ALLOWED_EXTENSIONS = config.get("system","ALLOWED_EXTENSIONS")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/process/<client>', methods=['POST'])
def upload_file(client):
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            # flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        event_id = request.form["event_id"]
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            updater.read_file(os.path.join(UPLOAD_FOLDER, filename), client, event_id)

            return jsonify(dict(success=True))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''
