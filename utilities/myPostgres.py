import psycopg2
import sys


class myPostgres():

    server = ""
    database = ""
    user = ""
    password = ""

    def __init__(self, server, database, user, password):
        self.server = server
        self.database = database
        self.user = user
        self.password = password

    def getdb(self):
        con = None
        # import pdb; pdb.set_trace()
        try:
            con = psycopg2.connect(
                host=self.server, dbname=self.database, user=self.user, password=self.password)
            return con

        except psycopg2.DatabaseError as e:
            print ('Error %s' % e)
            sys.exit(1)

    def get(self, query):
        try:
            con = self.getdb()
            cur = con.cursor()
            cur.execute(query)
            return cur.fetchall()
        except (psycopg2.DatabaseError, Exception) as ex:
            print (ex)
            sys.exit(1)
        finally:
            if con:
                con.close()

    def execute(self, query):
        try:
            con = self.getdb()
            cur = con.cursor()
            # logging.info('executing query')
            cur.execute(query)
            con.commit()
            # logging.info('query executed')
        except (psycopg2.DatabaseError, Exception) as ex:
            # logging.warning(ex)
            print (ex)
            sys.exit(1)
        finally:
            if con:
                con.close()
