from xlrd import open_workbook
from flask import jsonify
from utilities.mySQL import mySQL
from utilities.myPostgres import myPostgres


class BulkUpdater():

    def read_file(self, file_path, client, event_id):
        book = open_workbook(file_path)
        sheet = book.sheet_by_index(0)
        keys = [sheet.cell(
            0, col_index).value for col_index in range(sheet.ncols)]

        dict_list = []
        for row_index in range(1, sheet.nrows):
            d = {keys[col_index]: sheet.cell(row_index, col_index).value
                 for col_index in range(sheet.ncols)}
            dict_list.append(d)

        print(dict_list)
        self.update_db(dict_list, client, event_id)

    def update_db(self, dict_list, client, event_id):
        my = mySQL()
        query = """select server,db,user,password from settings where id like '{}'""".format(
            client)
        result = my.get(query)
        result = {key: value[0] for key, value in result.to_dict().items()}
        print(result)

        my = myPostgres(result['server'], result['db'], result[
                        'user'], result['password'])

        for item in dict_list:
            query = """Select id from areas where name like '{}';""".format(
                str(item["Areas"] if isinstance(item["Areas"], str) else int(item["Areas"])))
            area_id = my.get(query)[0][0]

            query = """Select id from products where name like '{}';""".format(
                str(item["Products"] if isinstance(item["Products"], str) else int(item["Products"])))
            product_id = my.get(query)[0][0]

            query = """Select id from data_group where product_id = {} and area_id = {};""".format(
                product_id, area_id)
            data_group_id = my.get(query)[0][0]

            query = """Update event_participants set tags = '{}%' where data_group_id = {} and event_id = {};""".format(int(item["Tags"]), data_group_id, event_id)
            my.execute(query)

            print(query)
