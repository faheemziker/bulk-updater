import mysql.connector
import logging

class mySQL():
    def getdb(self):
        try:
            logging.info('Connecting To Database')
            db = mysql.connector.connect(user='faheem', password='E57C2B36EAEF9C2F9F649DB5A8372', host='Mle.eplanner.io',database='eplanner', port=1114)
            if db:
                logging.info('Connectied to database')
            return db
        except Exception as ex:
            logging.warning(ex)
            import sys, traceback
            print(traceback.print_exc(file=sys.stdout))            
            exit(1)

    def get(self, query):
        import pandas as pd
        try:
            logging.info('getting db')
            db = self.getdb()
            logging.info('running query')
            logging.info(query)
            out = pd.read_sql(query, db)
            logging.info('query completed')
            return out;
        except Exception as ex:
            logging.warning(ex)
        finally:
            db.close()

    def execute(self, query, client=''):
        try:
            db = self.getdb()
            db.autocommit = True
            cur = db.cursor()
            logging.info('executing query')
            cur.execute(query);
            logging.info('query executed')
        except Exception as ex:
            logging.warning(ex)            
        finally:
            cur.close()
            db.close()
